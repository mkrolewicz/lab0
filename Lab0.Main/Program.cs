using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
        public class Studenci
        {
            public Studenci() { }

            public virtual int WplacCzesne(int kwota)
            {
                kwota = 1000;
                return kwota;
            }
        }
        public class Wieczorowi : Studenci
        {
            public Wieczorowi() : base() { }

            public override int WplacCzesne(int kwota)
            {
                kwota += 600;
                return kwota;
            }
        }

        public class Zaoczni : Studenci
        {
            public Zaoczni() : base() { }

            public override int WplacCzesne(int kwota)
            {
                kwota += 700;
                return kwota;
            }

        }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Aby sprawdzić wysokość czesnego, wybierz tryb studiów studenta:");
            Console.WriteLine("1 ) Wieczorowi");
            Console.WriteLine("2 ) Zaoczni");
            int wybor = Convert.ToInt32(Console.ReadLine());

            switch (wybor)
            {
                case 1:
                    Console.WriteLine("Wysokość czesnego wynosi 3000 PLN");
                    break;
                case 2:
                    Console.WriteLine("Wysokość czesnego wynosi 2000 PLN");
                    break;
            }
            Console.ReadKey();

        }
        
    }
}