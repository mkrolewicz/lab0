﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    public class LabDescriptor
    {
        public static Type A = typeof(Studenci);
        public static Type B = typeof(Wieczorowi);
        public static Type C = typeof(Zaoczni);

        public static string commonMethodName = "WplacCzesne";
    }
}
